package ru.altair.test.controller

import freemarker.template.Configuration
import freemarker.template.Template
import groovy.json.JsonException
import groovy.json.JsonSlurper
import org.json.JSONException
import org.json.XML
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import ru.altair.test.MyTemplateExceptionHandler

@Controller
class Test {

    @RequestMapping(value = "/xml", method = RequestMethod.POST)
    @ResponseBody
    public String xml(@RequestParam(value = "ftl") String ftl,
                      @RequestParam(value = "json") String data) {
        StringWriter writer = new StringWriter()
        def object;
        if (data.startsWith("<")) {
            try {
                data = XML.toJSONObject(data).toString();
            } catch (JSONException e) {
                writer.write("Invalid XML: " + e.getMessage())
                data = null;
            }
        }
        if(data!=null){
            def jsonSlurper = new JsonSlurper()
            try {
                object = jsonSlurper.parseText(data)
            } catch (JsonException e) {
                writer.write("Invalid JSON: " + e.getMessage())
                return writer.toString()
            }
            if (object != null) {
                Configuration config = new Configuration();
                config.setTemplateExceptionHandler(new MyTemplateExceptionHandler())
                Template template = new Template("t1", new StringReader(ftl), config)
                template.process(object, writer)
            } else {
                writer.write("Unknow eror when parsing data");
            }
        }
        writer.flush()
        return writer.toString()

    }
}