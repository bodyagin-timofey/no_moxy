package ru.altair.test

import freemarker.core.Environment
import freemarker.template.TemplateException
import freemarker.template.TemplateExceptionHandler

/**
 * Created by Tim on 04.11.2015.
 */
class MyTemplateExceptionHandler implements TemplateExceptionHandler{

    @Override
    void handleTemplateException(TemplateException te, Environment env, Writer writer) {
        try {
            writer.write("?");
            //writer.write("[ERROR: " + te.getMessage() + "]");
        } catch (IOException ex) {
            throw new TemplateException("Failed to print error message. Cause: " + ex, env);
        }
    }
}
